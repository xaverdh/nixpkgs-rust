mod result_ext;

use std::os::unix::ffi::{OsStrExt,OsStringExt};
use std::path::{Path,PathBuf};
use std::ffi::{OsStr,OsString};
use std::fs;
use std::fs::File;
use std::io::Write;
use std::os::unix;

use std::env;
use std::io::Result;
use std::process::Command;

use std::collections::HashSet;
use std::collections::HashMap;
use std::collections::VecDeque;

use users;
use crate::result_ext::ResultExt;


fn main() {
    let etc = Path::new("/etc");
    let etc_static = Path::new("/etc/static");
    let etc_clean = Path::new("/etc/.clean");
    let etc_nixos = Path::new("/etc/NIXOS");
    let etc_new = env::args_os().nth(1).unwrap();
    let etc_new = Path::new(&etc_new);

    atomic_symlink(&etc_new,etc_static)
        .expect("atomic symlink of etc failed");

    cleanup(etc,etc_static);

    let old_copied = read_etc_clean(etc_clean);
    let clean = fs::OpenOptions::new()
        .append(true).create(true)
        .open(etc_clean).unwrap();

    let copied : HashSet<OsString> =
        traverse( etc,
            |p,t| link(&clean,etc_static,etc_new,p,t)
        )
        .values()
        .filter_map( |x| x.as_ref().map(|p| p.to_path_buf().into_os_string()) )
        .collect();

    for file in old_copied.difference(&copied) {
        let file = etc.join(file);
        eprintln!("removing obsolete file {}...",&file.to_string_lossy());
        fs::remove_file(&file).unwrap();
    }
    write_etc_clean(&etc_clean,&copied).unwrap();
    write_nixos_tag(&etc_nixos).unwrap();
}


// Traverse a directory tree in DFS fashion, calling the provided function
// on every path encountered. Returns a HashMap with keys given by the
// paths visited and the corresponding values by the output of the
// function on them.
// Does not follow symlinks.
fn traverse<F : Fn(&Path,fs::FileType) -> T,T>(path : &Path,f : F) -> HashMap<PathBuf,T> {
    let mut visited = HashMap::new();
    let mut todo = VecDeque::new();
    let cpath = path.canonicalize().unwrap();
    todo.push_back( cpath );
    while let Some(p) = todo.pop_front() {
        let file_type = ftype(&p).unwrap();
        let res = f(&p,file_type);
        visited.insert(p.clone(),res);
        if file_type.is_dir(){
            for entry in fs::read_dir(&p).unwrap() {
                let e = entry.unwrap().path().canonicalize().unwrap();
                if !visited.contains_key(&e) {
                    todo.push_back(e);
                }
            }
        }
    }
    return visited;
}

// Check if a path points to files in /etc/static, meaning that either
// it is a symlink to a file in /etc/static or a directory with all
// children being static.
fn is_static(path : &Path) -> bool {
    let file_type = ftype(&path).unwrap();
    if file_type.is_symlink() {
        let target = fs::read_link(&path).unwrap();
        target.starts_with("/etc/static")
    } else if file_type.is_dir() {
        fs::read_dir(&path).unwrap()
            .all( |p| is_static(&p.unwrap().path()) )
    } else {
        false
    }
}

// Remove dangling symlinks that point to /etc/static. These are
// configuration files that existed in a previous configuration but not
// in the current one. For efficiency, don't look under /etc/nixos
// (where all the NixOS sources live).
fn cleanup(path : &Path,etc_static : &Path) {
    let path = path.as_ref();
    if path != Path::new("/etc/nixos") {
        let file_type = ftype(&path).unwrap();
        if file_type.is_symlink() {
            let target = fs::read_link(path).unwrap();
            if target.starts_with(etc_static) {
                let x = etc_static.join(
                        path.strip_prefix("/etc").unwrap()
                    );
                if !x.exists() || !ftype(&x).unwrap().is_symlink() {
                    eprintln!("removing obsolete symlink {}...",&path.to_string_lossy());
                    fs::remove_file(path).unwrap();
                }
            }
        } else if file_type.is_dir() {
            for entry in fs::read_dir(&path).unwrap() {
                let p = entry.unwrap().path();
                cleanup(&p,etc_static)
            }
        }
    }
}

// Read a metadata file from the etc in store.
// These are additional metadata files associated with a given
// file f proper. Currently there are three possible forms:
// * f._mode contains information on how to realise that file in /etc.
//   It may contain either the literal "direct-symlink", in which case
//   a symlink in /etc shall be created, pointing to f directly, i.e.
//   *not* via the /etc/static symlink), or a mode specification, in
//   which case a copy of f shall be created in /etc with the given mode.
//  * f._uid contains a user id specification, which is used in the
//   copy case from above.
//  * f._gid contains a group id specification, which is used in the
//   copy case from above.
fn read_etc_meta(postfix : &OsStr, path : &Path) -> Result<Vec<u8>> {
    let file = append(postfix,path);
    let mut meta = fs::read(file)?;
    meta.retain(|&c| c != b'\n');
    Ok(meta)
}

// Read the .clean file from /etc (usually /etc/.clean), returning a
// HashSet of entries. It keeps track of files that were copied in a
// given version of etc.
fn read_etc_clean(etc_clean : &Path) -> HashSet<OsString> {
    let mut s = HashSet::new();
    if let Ok(lines) = fs::read(etc_clean) {
        for line in lines.split(|c| c == &b'\n') {
            s.insert(OsString::from_vec(line.to_vec()));
        }
    }
    return s
}

fn write_etc_clean(etc_clean : &Path,
                   lines : &HashSet<OsString>) -> Result<()> {
    let mut s = OsString::new();
    for line in lines {
        s.push(line);
        s.push("\n");
    }
    fs::write(etc_clean,s.as_bytes())
}

fn write_nixos_tag(etc_nixos : &Path) -> Result<()> {
  fs::write(etc_nixos,b"")
}

// Given a specifications for user and group id, change the given file
// to have these ownership properties.
// The specification can be of the form
//  * '+' followed by bytes
//    In this case the bytes are interpreted as a an integer value.
//    encoded in the usual human readable fashion.
//  * something else
//    In this case the value is taken to be a user / group name and
//    looked up /etc/passwd or /etc/group.
fn chown_from_spec(user_spec : &Vec<u8>, group_spec : &Vec<u8>,
                   file : &Path) {
    let uid_str =
        if let Some(b'+') = user_spec.get(0) {
            OsString::from_vec( user_spec[1..].to_vec() )
        } else {
            let user = users::get_user_by_name(OsStr::from_bytes(user_spec)).unwrap();
            OsString::from( user.uid().to_string() )
        };
    let gid_str =
        if let Some(b'+') = group_spec.get(0) {
            OsString::from_vec( group_spec[1..].to_vec() )
        } else {
            let group = users::get_group_by_name(OsStr::from_bytes(group_spec)).unwrap();
            OsString::from( group.gid().to_string() )
        };
    let mut owner = OsString::new();
    owner.push(&uid_str);
    owner.push(OsStr::new(":"));
    owner.push(&gid_str);
    Command::new("chown")
        .args(&[ owner, file.to_path_buf().into_os_string() ])
        .spawn().ok();
}

fn chmod_from_spec(mode_spec : &Vec<u8>,path : &Path) {
    let mode = OsStr::from_bytes(mode_spec);
    Command::new("chmod")
        .args(&[ mode, OsStr::new(&path) ])
        .spawn().ok();
}

// For a given file in the etc tree, create a corresponding symlink
// in /etc to /etc/static. The indirection through /etc/static is to
// make switching to a new configuration somewhat more atomic.
fn link(mut clean : &File, etc_static : &Path, etc_new : &Path,
        path : &Path, file_type : fs::FileType) -> Option<PathBuf>{
    let file = path.strip_prefix(etc_new).ok()?;

    let target = Path::new("/etc").join(file);
    let source = etc_static.join(file);

    if let Some(dir) = target.parent() {
        fs::create_dir_all(&dir).unwrap();
    }

    if file_type.is_symlink()
        && ftype(&target).map_or(false,|p| p.is_dir()) {
            if is_static(&target) {
                fs::remove_dir_all(&target).or_warn();
            } else {
                eprintln!("{} directory contains user files. Symlinking may fail.",&target.to_string_lossy());
            }
    }

    if append(OsStr::new(".mode"),&path).exists() {
        let mode_spec = read_etc_meta(OsStr::new(".mode"),&path).unwrap();
        if OsStr::from_bytes(&mode_spec) == OsStr::new("direct-symlink") {
            let real_source = fs::read_link(&source).unwrap();
            atomic_symlink(&real_source,&target).or_warn();
        } else {
            let user_spec = read_etc_meta(OsStr::new(".uid"),&path).unwrap();
            let group_spec = read_etc_meta(OsStr::new(".gid"),&path).unwrap();
            let tmp = append(OsStr::new(".tmp"),&target);
            fs::copy(&source,&tmp).or_warn();
            chown_from_spec(&user_spec,&group_spec,&tmp);
            chmod_from_spec(&mode_spec,&tmp);
            fs::rename(&tmp,&target).or_warn();
            fs::rename(&tmp,&target).or_warn();
        };
        let mut line = file.to_path_buf().into_os_string();
        line.push("\n");
        clean.write_all(line.as_bytes()).unwrap();
        Some(file.to_path_buf())
    } else {
        if file_type.is_symlink() {
            atomic_symlink(&source,&target).or_warn();
        };
        None
    }
}


/* auxiliary stuff */

// Atomically update the symlink at target to point to source.
fn atomic_symlink(source : &Path,target : &Path) -> Result<()> {
    let tmp = append(OsStr::new(".tmp"),&target);
    fs::remove_file(&tmp).ok();
    unix::fs::symlink(&source,&tmp)?;
    fs::rename(&tmp,&target)?;
    Ok(())
}

fn alter_name<F : Fn(OsString) -> OsString>(f : F, path : &Path) -> PathBuf {
    let name = path.file_name()
        .map(|s| s.clone().to_os_string() )
        .unwrap_or(OsString::new());
    path.with_file_name(f(name))
}

fn append(postfix : &OsStr, path : &Path) -> PathBuf {
    alter_name(|mut name| { name.push(postfix); name }, &path)
}

fn ftype(path : &Path) -> Result<fs::FileType> {
    fs::symlink_metadata(&path).map(|p| p.file_type())
}


