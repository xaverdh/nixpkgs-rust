pub trait ResultExt<T,E> {
    fn or_warn(&self) -> Option<&T>;
}

impl<T,E> ResultExt<T,E> for std::result::Result<T,E>
    where E : std::fmt::Display {
    fn or_warn(&self) -> Option<&T> {
        self.as_ref().map(|v| Some(v))
            .unwrap_or_else( |e| { eprintln!("{}",e); None } )
    }
}
