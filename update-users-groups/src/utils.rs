use std::hash::Hash;
use std::collections::HashMap;

use std::path::Path;
use std::io::Write;
use std::ffi::OsString;
use std::os::unix::ffi::OsStringExt;
use std::os::unix::ffi::OsStrExt;
use std::fs::{Permissions};
use std::os::unix::fs::{PermissionsExt};
use std::os::unix as unix;
use std::os::fd::AsRawFd;

use nix;
use nix::{NixPath};
use nix::unistd::LinkatFlags;

use std::process::{Command,ExitStatus};
use tempfile;


// Given a file path and some data, try to atomically write it to a
// file at that path.
// TODO: * better error propagation
pub fn atomic_write(path : &Path, data : &[u8], opt_mode : Option<u32>, uid: Option<u32>, gid: Option<u32>)
    -> Option<()> {
    let target_dir = &path.parent().unwrap();
    let mut tmp = tempfile::tempfile_in(&target_dir).ok()?;
    tmp.write_all(&data).ok()?;
    if let Some(mode) = opt_mode {
        tmp.set_permissions(Permissions::from_mode(mode)).ok()?;
    }
    unix::fs::fchown(&tmp,uid,gid).ok()?;

    // Use /proc/self/fd/ to get a file path for the unnamed file
    // On linux it would be preferrable to use AT_EMPTY_PATH wiht unlink
    // instead, but unix / nix crates don't expose it.
    let tmp_path = OsString::from(format!("/proc/self/fd/{}", tmp.as_raw_fd()));
    nix::unistd::linkat(
      None,
      tmp_path.as_os_str(),
      None,
      path.as_os_str(),
      LinkatFlags::SymlinkFollow
    ).ok()?;
    Some(())
}

pub fn parse_lines(
    data : &Vec<u8>
    ) -> Vec<&[u8]> {
    data.split(|c| *c == b'\n')
        .filter(|line| !line.is_empty())
        .collect()
}

pub fn bytes_to_os(bs : &[u8]) -> OsString {
    OsString::from_vec(bs.to_vec())
}

pub fn join_os(items : &[OsString], sep : &OsString) -> OsString {
    let mut iter = items.iter();
    if let Some(init) = iter.next() {
        let buf_lengths : usize = items.iter().map(|s| s.len()).sum();
        let cap : usize = buf_lengths + sep.len() * (items.len()-1);
        let mut res = OsString::with_capacity(cap);
        res.push(init);
        for item in iter {
            res.push(sep);
            res.push(item);
        }
        res
    } else {
      OsString::new()
    }
}
