use crate::ids as ids;
use ids::{IdAllocator,Id};

use crate::etc;
use etc::{User,Group,ShadowData};

use crate::spec;
use spec::{UserSpec,GroupSpec,UsersGroupsSpec,IdRange};

use crate::nixos;
use crate::utils;

use std::path::{Path,PathBuf};
use std::ffi::{OsStr,OsString};
use std::os::unix::ffi::OsStringExt;
use std::fs::Permissions;
use std::os::unix::fs::PermissionsExt;
use std::os::unix as unix;
use std::process::Command;
use std::env;
use std::fs;

use std::collections::HashMap;
use std::collections::HashSet;

use std::convert::TryInto;

use sha_crypt::{Sha512Params,CryptError};

pub trait World {
    fn invalidate(target : &str);
    fn uid_exists(id : Id) -> bool;
    fn gid_exists(id : Id) -> bool;
    fn create_home_dir(path : &Path, uid : u32, gid : u32);
}

pub struct MockWorld {}
pub struct RealWorld {}

impl World for MockWorld {
    fn invalidate(_ : &str) { }
    fn uid_exists(_ : Id) -> bool { false }
    fn gid_exists(_ : Id) -> bool { false }
    fn create_home_dir(_ : &Path, _ : u32, _ : u32) { }
}

impl World for RealWorld {
    fn invalidate(target : &str) {
        let st = Command::new("nscd").args(&["--invalidate", target])
            .status().expect("failed to execute nscd");
        if ! st.success() {
            eprintln!("warning: nscd failed to invalidate /etc/{}",target);
        }
    }
    fn create_home_dir(path : &Path, uid : u32, gid : u32){
        if !path.exists() {
            fs::create_dir_all(&path).unwrap();
        }
        unix::fs::chown(&path,Some(uid),Some(gid)).unwrap();
        fs::set_permissions(&path,Permissions::from_mode(0o700))
            .unwrap();
    }
    fn uid_exists(id : Id) -> bool {
        users::get_group_by_gid(id).is_some()
    }
    fn gid_exists(id : Id) -> bool {
        users::get_user_by_uid(id).is_some()
    }
}

pub fn update_users_groups<W : World>(
    spec_file : &Path,
    var_nixos : &Path,
    etc_path : &Path
    ) {

    let nixos = nixos::NixOSVar::new(var_nixos);
    let mut uid_map = nixos.read_uids();
    let mut gid_map = nixos.read_gids();
    let mut subuid_map = nixos.read_subuids();

    // Read the declarative specification of users and groups.
    let spec = spec::read_from(spec_file);

    // Read current user / group / shadow data from /etc
    let etc = etc::Etc::new(etc_path);
    let current_groups = etc.read_groups().unwrap();
    let current_users = etc.read_users().unwrap();
    let shadow_data = etc.read_shadow().unwrap();

    // These sets keep track of the *delarative* subset of users
    // and groups used in the history of the system (both current
    // as well as deleted ones). This data needs to be tracked
    // seperately, so we know if we should delete a user / group
    // if its no longer in the new declarative specification.
    // If it used to be a declarative user / group, we want to
    // remove it from the system, otherwise we should not touch it.
    // (cf. compute_mutable_{groups,users})
    let decl_groups = nixos.read_groups().unwrap();
    let decl_users = nixos.read_users().unwrap();

    let mut new_groups = compute_groups::<W>(
        &spec, &current_groups, &decl_users, &gid_map
        );
    // Update the gid map to include the new groups.
    for (name,group) in &new_groups {
        gid_map.insert(name.clone(),group.id);
    }

    nixos.write_groups(
        &new_groups.keys().cloned().collect()
        ).unwrap();

    new_groups.extend(
        compute_mutable_groups(
            &new_groups, &current_groups,
            &decl_groups, spec.mutable_users
            )
        );

    etc.write_groups(&new_groups);
    nixos.write_gids(&gid_map);
    W::invalidate("group");

    let (mut new_users,new_passwords) = compute_users::<W>(
        &spec, &current_users, &uid_map, &new_groups
        );
    // Update the uid map to include data from the new users.
    for (name,user) in &new_users {
        uid_map.insert(name.clone(),user.id);
    }

    create_home_dirs::<W>(&spec.users, &uid_map, &gid_map);

    nixos.write_users(
        &new_users.keys().cloned().collect()
        ).unwrap();

    new_users.extend(
        compute_mutable_users(
            &new_users, &current_users,
            &decl_users, spec.mutable_users
            )
        );

    etc.write_users(&new_users);
    nixos.write_uids(&uid_map);
    W::invalidate("passwd");

    // Merge new declaratively specified passwords with
    // existing shadow data from /etc/shadow.
    let shadow_data = compute_shadow(
        new_passwords,
        shadow_data,
        &new_users,
        spec.mutable_users
        );

    etc.write_shadow(&shadow_data);

    let (subuids,subgids) = compute_subuids::<W>(
        &spec.users,
        &subuid_map,
        );
    for (name,ranges) in &subuids {
        for range in ranges {
            subuid_map.insert(name.clone(),range.start_id);
        }
    }

    etc.write_subuids(&subuids);
    etc.write_subgids(&subgids);
    nixos.write_subuids(&subuid_map);

}


// Collect all those preexisting groups from /etc/group not yet present
// in the new group data, that should be kept.
// More specifically we want to keep those groups which were not
// marked as declarative (and only if we allow mutable groups at all).
fn compute_mutable_groups(
    new_groups : &HashMap<String,Group>,
    current_groups : &HashMap<String,Group>,
    decl_groups : &HashSet<String>,
    mutable_users : bool
    )
    -> HashMap<String,Group> {

    current_groups.iter().filter_map(|(name,group)| {
        if !new_groups.contains_key(name) {
            if mutable_users && !decl_groups.contains(name) {
                return Some(( name.clone(), group.clone() ))
            } else {
                eprintln!("marking group {} for removal",&name);
            }
        }
        None
    }).collect()
}

// Collect all those preexisting users from /etc/passwd data
// not yet present in the new user data, that should be kept.
// More specifically we want to keep those users which were not
// marked as declarative (and only if we allow mutable users at all).
fn compute_mutable_users(
    new_users : &HashMap<String,User>,
    current_users : &HashMap<String,User>,
    decl_users : &HashSet<String>,
    mutable_users : bool
    )
    -> HashMap<String,User> {

    current_users.iter().filter_map(|(name,user)| {
        if !new_users.contains_key(name) {
            if mutable_users && !decl_users.contains(name) {
                return Some(( name.clone(), user.clone() ))
            } else {
                eprintln!("marking user {} for removal",&name);
            }
        }
        None
    }).collect()
}


// Ensure home directores are present with the correct ownership
// and permissions for all users.
fn create_home_dirs<W : World>(
    user_specs : &Vec<UserSpec>,
    uid_map : &HashMap<String,Id>,
    gid_map : &HashMap<String,Id>
    ) {

    for user in user_specs.iter().filter(|u| u.create_home) {
        let path = Path::new(&user.home);
        let uid = *uid_map.get(&user.name).unwrap();
        let gid = *gid_map.get(&user.group).unwrap();
        W::create_home_dir(&path,uid,gid)
    }
}

fn compute_groups<W : World>(
    spec : &UsersGroupsSpec,
    current_groups : &HashMap<String,Group>,
    decl_users : &HashSet<String>,
    gid_map : &HashMap<String,Id>
    )
    -> HashMap<String,Group> {

    // Collect the previously used group ids
    let prev_gids : HashSet<Id> = gid_map.values().map(|v| *v).collect();

    // Collect used group ids from both
    // the manually assigned ids from the users-groups spec
    // as well as the current /etc/groups
    let gids_used : HashSet<Id> = spec.groups.iter()
        .filter_map(|v| v.gid)
        .chain(current_groups.values().map(|g| g.id))
        .collect();
    let mut al = ids::IdAllocator::new(
        1,
        // Forbid allocating gids currently or previously in use.
        gids_used.union(&prev_gids).map(|id|*id).collect(),
        400,
        999
        );

    spec.groups.iter().map(|group_spec| {
        let (name,new_group) = compute_new_group(
            &group_spec,
            current_groups,
            decl_users,
            spec.mutable_users,
            |name| allocate_gid::<W>(&mut al, &name, &gid_map)
            );
        (name,new_group)
    }).collect()
}

fn compute_new_group<F : FnMut(&str) -> Option<Id>> (
    group_spec : &GroupSpec,
    current_groups : &HashMap<String,Group>,
    decl_users : &HashSet<String>,
    mutable_users : bool,
    mut alloc_id : F,
    )
    -> (String,Group) {

    let name = &group_spec.name;
    let mut members : HashSet<String> = group_spec.members.iter()
        .map(|s| s.clone()).collect();
    let new_group = if let Some(existing) = current_groups.get(name) {
        let new_id = group_spec.gid.unwrap_or_else(|| existing.id);
        if new_id != existing.id {
            eprintln!("warning: not applying GID change of group {} ({} -> {})",name,existing.id,new_id);
        }
        let new_password = &existing.password; // do we want this?
        if mutable_users {
            // merge in non-declarative group members
            for uname in &existing.members {
                if ! decl_users.contains(uname){
                    members.insert(uname.clone());
                }
            }
        }
        Group {
            password: new_password.clone(),
            id: new_id,
            members: members.into_iter().collect()
        }
    } else {
        let new_id = group_spec.gid.unwrap_or_else(
            || alloc_id(&name).expect("ran out of gids")
        );
        Group {
            password: utils::bytes_to_os(b"x"),
            id: new_id,
            members: group_spec.members.clone()
        }
    };
    ( name.clone(), new_group )
}

fn compute_users<W : World>(
    spec : &UsersGroupsSpec,
    current_users : &HashMap<String,User>,
    uid_map : &HashMap<String,Id>,
    new_groups : &HashMap<String,Group>
    )
    -> ( HashMap<String,User>, HashMap<String,Option<String>> ) {

    // Collect the previously used user ids
    let prev_uids: HashSet<Id> = uid_map.values().cloned().collect();

    // Collect used user ids from both
    // the manually assigned ids from the users-groups spec
    // as well as the current /etc/passwd
    let uids_used : HashSet<Id> = spec.users.iter()
        .filter_map(|v| v.uid)
        .chain(current_users.values().map(|u| u.id))
        .collect();

    let mut al = ids::IdAllocator::new(
        1,
        // Forbid allocating uids currently or previously in use.
        uids_used.union(&prev_uids).map(|id|*id).collect(),
        1000,
        29999
        );

    spec.users.iter().map(|user_spec| {
        let (name,new_user,hashed_pass) = compute_new_user(
            &user_spec,
            current_users,
            new_groups,
            |name,is_sys_user| allocate_uid::<W>(
                &mut al, &name, is_sys_user, &uid_map
                )
            );
        ( (name.clone(),new_user), (name,hashed_pass) )
    }).unzip()
}


fn compute_new_user<F : FnMut(&str,bool) -> Option<Id>> (
    user_spec : &UserSpec,
    current_users : &HashMap<String,User>,
    new_groups : &HashMap<String,Group>,
    mut alloc_id : F,
    )
    -> (String,User,Option<String>) {

    let name = &user_spec.name;

    let group_str = &user_spec.group;
    // resolve the gid of the user
    let new_gid = ids::parse_id(&group_str)
        .or(new_groups.get(group_str).map(|g| g.id))
        .unwrap_or_else(|| {
            eprintln!("warning: user {} has unknown group {}",name,group_str);
            nixos::NOGROUP_GID
        });

    let prior = current_users.get(name);

    let new_id = if let Some(existing) = prior {
        let new_id = user_spec.uid.unwrap_or_else(|| existing.id);
        if new_id != existing.id {
            eprintln!("warning: not applying UID change of user {} ({} -> {})",name,existing.id,new_id);
            existing.id
        } else {
            new_id
        }
    } else {
        user_spec.uid.unwrap_or_else(
            || alloc_id(&name,user_spec.is_system_user)
                .expect("ran out of user ids")
        )
    };

    let hashed_pass = compute_hashed_password(&user_spec,prior.is_some());

    let new_shell = compute_user_shell(&user_spec,&prior);

    let fake_pass = prior
        .map(|ex| ex.fake_password.clone())
        .unwrap_or(utils::bytes_to_os(b"x"));

    let user = User {
        fake_password: fake_pass,
        id: new_id,
        group_id: new_gid,
        description: OsString::from(user_spec.description.clone()),
        home: OsString::from(user_spec.home.clone()),
        shell: new_shell,
    };
    ( name.clone(), user, hashed_pass )
}

fn compute_subuids<W : World>(
    user_specs : &Vec<UserSpec>,
    subuid_map : &HashMap<String,Id>
    )
    -> ( Vec<(String,Vec<IdRange>)>, Vec<(String,Vec<IdRange>)> ) {

    // Collect the previously used sub uids
    let prev_subuids : HashSet<Id> = subuid_map.values().map(|v| *v).collect();

    let mut al = ids::IdAllocator::new(
        nixos::UID_COUNT,
        // Forbid allocating subuids previously in use.
        prev_subuids,
        nixos::SUBUID_INDEX,
        nixos::SUBUID_INDEX * 100 // TODO: No upper bounds?
        );

    user_specs.iter().map(|user_spec| {
        let name = &user_spec.name;

        let mut uid_ranges : Vec<IdRange> = user_spec.subuid_ranges
            .iter().cloned().collect();
        let mut gid_ranges : Vec<IdRange> = user_spec.subgid_ranges
            .iter().cloned().collect();
        if user_spec.is_normal_user {
            let subordinate = allocate_subuid::<W>(
                &mut al, &name, &subuid_map
                ).expect("ran out of user ids");
            let range = IdRange {
                start_id: subordinate,
                count: nixos::UID_COUNT,
            };
            uid_ranges.push(range.clone());
            gid_ranges.push(range.clone());
        }
        ( (name.clone(),uid_ranges), (name.clone(),gid_ranges) )
    }).unzip()
}

// Compute the hashed password for use in /etc/shadow.
// If the user already exists on the system, look at the
// "passwordFile" and literal "password" option from the declarative
// user specification. If setting up a new user, also take the
// "initialPassword" and "initialHashedPassword" options into account.
fn compute_hashed_password(user_spec : &UserSpec, user_exists : bool)
    -> Option<String> {

    if let Some(pass_file) = &user_spec.password_file {
        let path = Path::new(&pass_file);
        if path.exists() {
            return Some(fs::read_to_string(&path).unwrap()
                        .trim_end_matches("\n").to_string())
        } else {
            eprintln!("warning: password file {} does not exist",&pass_file);
        }
    } else if let Some(pass) = &user_spec.password {
        return Some(hash_password(&pass).unwrap())
    } else if !user_exists {
        if let Some(initial) = &user_spec.initial_password {
            return Some(hash_password(&initial).unwrap())
        } else if let Some(hashed) = &user_spec.hashed_password {
            return Some(hashed.to_string())
        }
        return None
    }
    None
}


// Compute the (new) shell for a given declarative user.
// If the shell is not given as part of the user spec
// (i.e. set to null), and the user exists, try to reuse the old value.
fn compute_user_shell(user_spec : &UserSpec,prior : &Option<&User>)
    -> OsString {

    if let Some(shell) = &user_spec.shell {
        OsString::from(shell.clone())
    } else if let Some(existing) = prior {
        OsString::from(existing.shell.clone())
    } else {
        eprintln!("warning: no declarative or previous shell for {}, setting shell to nologin",&user_spec.name);
        OsString::from("/run/current-system/sw/bin/nologin")
    }
}

fn compute_shadow(
    new_passwords : HashMap<String,Option<String>>,
    mut current_shadow: HashMap<String,ShadowData>,
    new_users : &HashMap<String,User>,
    mutable_users : bool
    )
    -> HashMap<String,ShadowData> {

    // Drop all entries concerning users no longer present.
    current_shadow.retain(|name,_| new_users.contains_key(name) );

    let locked = String::from("!");

    if !mutable_users {
        // Update hashed passwords from the spec, falling back to "!"
        for (name,pw_data) in current_shadow.iter_mut() {
            let hashed = new_passwords.get(name)
                .cloned().flatten().unwrap_or(locked.clone());
            *pw_data = ShadowData {
                hashed_password: hashed.clone(),
                extra: pw_data.extra.clone()
            }
        }
    }

    // Create new entries in shadow for users which don't
    // have one already.
    for (name,_user) in new_users {
        if !current_shadow.contains_key(name) {
            let hashed = new_passwords.get(name)
                .cloned().flatten().unwrap_or(locked.clone());
            let pw_data = ShadowData {
                hashed_password: hashed.clone(),
                extra: OsString::from("1::::::")
            };
            current_shadow.insert(name.clone(),pw_data);
        }
    }
    current_shadow
}


// Allocate a user id for a given user.
// Tries to reuse the old id if the user already exists or once
// existed on the system.
fn allocate_uid<W : World>(
    al : &mut IdAllocator,
    name : &str, is_system_user : bool,
    uid_map : &HashMap<String,Id>
    )
    -> Option<Id> {

    if let Some(id) = uid_map.get(name).and_then(
        |id| al.try_allocate(*id)
    ) {
        eprintln!("reviving user '{}' with UID {}",name,id);
        Some(id)
    } else {
        if is_system_user {
            al.allocate_down(|id| users::get_user_by_uid(id).is_none())
        } else {
            al.allocate_up(|id| users::get_user_by_uid(id).is_none())
        }
    }
}

// Allocate a group id for a given group.
// Tries to reuse the old id if the group already exists or once
// existed on the system.
fn allocate_gid<W: World>(
    al : &mut IdAllocator,
    name : &str,
    gid_map : &HashMap<String,Id>
    )
    -> Option<Id> {

    if let Some(id) = gid_map.get(name).and_then(
        |id| al.try_allocate(*id)
    ) {
        eprintln!("reviving group '{}' with GID {}",name,id);
        Some(id)
    } else {
        al.allocate_down(|id| !W::gid_exists(id))
    }
}


fn allocate_subuid<W : World>(
    al : &mut IdAllocator,
    name : &str,
    subuid_map : &HashMap<String,Id>
    )
    -> Option<Id> {

    if let Some(id) = subuid_map.get(name).and_then(
        |id| al.try_allocate(*id)
    ) {
        Some(id)
    } else {
        al.allocate_up(|id| !W::uid_exists(id))
    }
}


// Hash a password with randomly generated salt using sha256 conforming
// to format of /etc/shadow.
fn hash_password(password : &str) -> Result<String,CryptError> {
    sha_crypt::sha512_simple(&password,&Default::default())
}


