// Handling of passwd, group and shadow
use crate::ids;
use crate::utils;

use ids::Id;
use crate::spec::IdRange;

use std::fs;
use std::str;
use std::io::Result;
use std::path::{Path,PathBuf};
use std::ffi::OsString;
use std::os::unix::ffi::OsStringExt;
use std::os::unix as unix;

use std::collections::HashMap;

use std::convert::TryFrom;

#[derive(Debug,Clone)]
pub struct Group {
    pub password : OsString,
    pub id : Id,
    pub members : Vec<String>,
}

#[derive(Debug,Clone)]
pub struct User {
    pub fake_password : OsString,
    pub id : Id,
    pub group_id : Id,
    pub description : OsString,
    pub home : OsString,
    pub shell : OsString,
}

pub struct ShadowData {
    pub hashed_password: String,
    pub extra: OsString,
}


pub struct Etc {
    etc : PathBuf
}

impl Etc {

    pub fn new<P : AsRef<Path>>(path : P) -> Etc {
        Etc { etc: path.as_ref().to_path_buf() }
    }

    pub fn read_groups(&self) -> Result<HashMap<String,Group>>{
        let path = self.etc.join("group");
        let data = fs::read(&path)?;
        Ok(
            parse_group(&data,&path)
            .expect(&is_unreadable(&path))
            )
    }

    pub fn read_users(&self) -> Result<HashMap<String,User>> {
        let path = self.etc.join("passwd");
        let data = fs::read(&path)?;
        Ok(
            parse_passwd(&data,&path)
            .expect(&is_unreadable(&path))
            )
    }

    pub fn read_shadow(&self) -> Result<HashMap<String,ShadowData>> {
        let path = self.etc.join("shadow");
        let data = fs::read(&path)?;
        Ok(
            parse_shadow(&data,&path)
            .expect(&is_unreadable(&path))
            )
    }


    pub fn write_groups(&self, new_groups : &HashMap<String,Group>)
        -> Option<()> {
        write_lines(
            new_groups.iter().map(fmt_group).collect(),
            &self.etc.join("group"),
            Some(0o644),
            None,
            None
            )
    }

    pub fn write_users(&self, new_users : &HashMap<String,User>)
        -> Option<()> {
        write_lines(
            new_users.iter().map(fmt_user).collect(),
            &self.etc.join("passwd"),
            Some(0o644),
            None,
            None
            )
    }

    pub fn write_shadow(&self, new_shadow : &HashMap<String,ShadowData>)
        -> Option<()> {
        let path = &self.etc.join("shadow");
        let uid = users::get_user_by_name("root")
            .map(|u| u.uid()).unwrap_or(0);
        let gid = users::get_group_by_name("passwd")
            .map(|g| g.gid()).unwrap_or_else(
                || users::get_group_by_name("root").map(|g| g.gid())
                .unwrap_or(0)
            );
        write_lines(
            new_shadow.iter().map(fmt_shadow_item).collect(),
            &path,
            Some(0o600),
            Some(uid),
            Some(gid)
        )
    }

    pub fn write_subuids(&self,
                         subuids : &Vec<(String,Vec<IdRange>)>
                         ) {
        write_sub_id_file(&self.etc.join("subuid"),&subuids);
    }

    pub fn write_subgids(&self,
                         subgids : &Vec<(String,Vec<IdRange>)>
                         ) {
        write_sub_id_file(&self.etc.join("subgid"),&subgids);
    }

}

fn parse_group(data : & Vec<u8>, location : &Path)
    -> Option<HashMap<String,Group>> {

    parse_colon_file::<4>(&data,&location).iter().map(|entries| {
        let name = from_utf8(entries[0],location);
        let pass = utils::bytes_to_os(entries[1]);
        let gid = id_from_bytes(entries[2],location)?;
        let members = entries[3].split(|c| *c == b',')
            .map(|bs| from_utf8(bs,location))
            .collect();
        let group = Group {
            password: pass,
            id: gid,
            members: members
        };
        Some(( name, group ))
    }).collect()
}

fn parse_passwd(data : &Vec<u8>, location : &Path)
    -> Option<HashMap<String,User>> {

    parse_colon_file::<7>(&data,&location).iter().map(|entries| {
        let name = from_utf8(entries[0],location);
        let fake_pass = utils::bytes_to_os(entries[1]);
        let uid = id_from_bytes(entries[2],location)?;
        let gid = id_from_bytes(entries[3],location)?;
        let desc = utils::bytes_to_os(entries[4]);
        let home = utils::bytes_to_os(entries[5]);
        let shell = utils::bytes_to_os(entries[6]);
        let user = User {
            fake_password: fake_pass,
            id: uid,
            group_id: gid,
            description: desc,
            home: home,
            shell: shell,
        };
        Some((name, user))
    }).collect()
}

fn parse_shadow(data : &Vec<u8>, location : &Path)
    -> Option<HashMap<String,ShadowData>> {

    parse_colon_file::<3>(&data,&location).iter().map(|entries| {
        let name = from_utf8(entries[0],location);
        let hashed_pass = from_utf8(entries[1],location);
        let extra = utils::bytes_to_os(entries[2]);
        let pass_data = ShadowData {
            hashed_password: hashed_pass,
            extra: extra
        };
        Some((name, pass_data))
    }).collect()
}

fn parse_colon_file<'a,const N: usize>(
    data : &'a Vec<u8>,
    location : &'a Path
    ) -> Vec<[&'a [u8];N]> {
    let err = format!("invalid file {}",location.to_string_lossy());
    utils::parse_lines(&data)
        .iter()
        .map( |line| <[&[u8];N]>::try_from(
                line.splitn(N,|c| *c == b':').collect::<Vec<&[u8]>>()
                ).expect(&err)
            )
        .collect()
}

fn write_sub_id_file(path : &Path,
                         ranges : &Vec<(String,Vec<IdRange>)>)
    -> Option <()> {
    write_lines(
        ranges.iter().map(fmt_sub_id).collect(),
        path,
        Some(0o644),
        None,
        None
        )
}

fn write_lines(
    lines : Vec<OsString>,
    path : &Path,
    opt_mode : Option<u32>,
    opt_uid : Option<u32>,
    opt_gid : Option<u32>
    ) -> Option<()> {
    let data = utils::join_os(
        &lines,
        &OsString::from("\n")
        ).into_vec();
    utils::atomic_write(&path, &data, opt_mode,opt_uid,opt_gid)
}

fn fmt_group((name,group) : (&String,&Group)) -> OsString {
    let Group { password: pass, id, members } = group;
    let sep = OsString::from(":");
    let mut data = OsString::new();
    data.push( OsString::from(name) );
    data.push(&sep);
    data.push( pass );
    data.push(&sep);
    data.push( OsString::from(id.to_string()) );
    data.push(&sep);
    let tail : Vec<OsString> = members.iter()
        .map(|s| OsString::from(s.clone())).collect();
    data.push( utils::join_os( tail.as_slice(), &sep) );
    data
}

fn fmt_shadow_item((name,item): (&String,&ShadowData)) -> OsString {
    let ShadowData { hashed_password: hashed, extra } = item;
    let sep = OsString::from(":");
    utils::join_os(
        &[ OsString::from(name)
        , OsString::from(hashed)
        , extra.clone() ],
        &sep
        )
}

fn fmt_user((name,user) : (&String,&User)) -> OsString {
    let User {
        fake_password: fake_pass,
        id,
        group_id: gid,
        description: desc,
        home,
        shell
    } = user;
    let sep = OsString::from(":");
    utils::join_os(
        &[ OsString::from(name)
        , fake_pass.clone()
        , OsString::from(id.to_string())
        , OsString::from(gid.to_string())
        , desc.clone()
        , home.clone()
        , shell.clone() ],
        &sep
        )
}


fn fmt_sub_id((name,ranges) : &(String,Vec<IdRange>)) -> OsString {
    let sep = OsString::from(":");
    let lines : Vec<OsString> = ranges.iter().map(|range|{
        let IdRange { start_id, count } = range;
        utils::join_os(
            &[ OsString::from(name)
            , OsString::from(start_id.to_string())
            , OsString::from(count.to_string()) ],
            &sep
            )
    }).collect();
    utils::join_os(&lines,&sep)
}

fn from_utf8(bytes : &[u8], location : &Path) -> String {
    let err = format!("invalid unicode (utf8) encountered in {} ",location.to_string_lossy());
    str::from_utf8(&bytes)
        .expect(&err)
        .to_string()
}

fn id_from_bytes(bytes : &[u8], location : &Path) -> Option<Id> {
    ids::parse_id( &from_utf8(&bytes,&location) )
}

fn is_unreadable(path : &Path) -> String {
    let s = path.to_string_lossy();
    format!("failed to parse {}",&s)
}
