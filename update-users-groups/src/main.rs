use users_groups::update::{update_users_groups,World,RealWorld,MockWorld};
use std::path::{Path,PathBuf};
use std::ffi::OsString;

use clap::{Arg, App};

fn main(){
    let default_etc = OsString::from("/etc");
    let default_var_nixos = OsString::from("/var/lib/nixos");
    let matches = App::new("update-users-groups")
        .arg(Arg::with_name("spec")
             .index(1)
             .required(true))
        .arg(Arg::with_name("etc")
             .long("etc")
             .help("path to etc directory")
             .default_value_os(&default_etc))
        .arg(Arg::with_name("var-nixos")
             .long("var-nixos")
             .help("path to nixos state files")
             .default_value_os(&default_var_nixos))
        .arg(Arg::with_name("timid")
             .long("timid")
             .help("Interact with the outside world as little as \
                    possible, i.e. only by reading and writing files \
                    in the provided etc and nixos state directories.")
             .takes_value(false))
        .get_matches();
    let spec_file = Path::new(matches.value_of("spec").unwrap());
    let var_nixos = Path::new(matches.value_of("var-nixos").unwrap());
    let etc_path = Path::new(matches.value_of("etc").unwrap());

    if matches.is_present("timid") {
        update_users_groups::<MockWorld>(
            &spec_file, &var_nixos, &etc_path
        );
    } else {
        update_users_groups::<RealWorld>(
            &spec_file, &var_nixos, &etc_path
        );
    }
}


