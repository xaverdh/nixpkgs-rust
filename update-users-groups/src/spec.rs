// Handling of users-groups.json
use crate::ids::Id;
use std::fs;
use std::path::Path;
use std::collections::HashMap;

use serde_json;
use serde::Deserialize;

#[derive(Debug,Clone)]
#[derive(Deserialize)]
pub struct IdRange {
    #[serde(alias="startGid")]
    #[serde(rename="startUid")]
    pub start_id : Id,

    pub count : u32,
}

#[derive(Debug)]
#[derive(Deserialize)]
pub struct UserSpec {
    pub name : String,
    pub uid: Option<Id>,
    pub group: String,

    #[serde(rename="isSystemUser")]
    pub is_system_user: bool,

    #[serde(rename="createHome")]
    pub create_home: bool,

    pub password : Option<String>,

    pub home : String,

    pub description : String,

    pub shell : Option<String>,

    #[serde(rename="passwordFile")]
    pub password_file : Option<String>,

   #[serde(rename="hashedPassword")]
    pub hashed_password : Option<String>,

    #[serde(rename="initialHashedPassword")]
    pub initial_hashed_password: Option<String>,

    #[serde(rename="initialPassword")]
    pub initial_password: Option<String>,

    #[serde(rename="isNormalUser")]
    pub is_normal_user: bool,

    #[serde(rename="subUidRanges")]
    pub subuid_ranges: Vec<IdRange>,

    #[serde(rename="subGidRanges")]
    pub subgid_ranges: Vec<IdRange>,

    #[serde(flatten)]
    pub extra : HashMap<String,serde_json::Value>,
}


#[derive(Debug)]
#[derive(Deserialize)]
pub struct GroupSpec {
    pub name : String,
    pub gid: Option<Id>,
    pub members : Vec<String>,
    #[serde(flatten)]
    pub extra : HashMap<String,serde_json::Value>,
}

#[derive(Debug)]
#[derive(Deserialize)]
pub struct UsersGroupsSpec {
    #[serde(rename = "mutableUsers")]
    pub mutable_users : bool,
    pub users : Vec<UserSpec>,
    pub groups : Vec<GroupSpec>,
}

// Read users-groups.json spec from given path and parse
// into a UsersGroupsSpec.
pub fn read_from(path : &Path) -> UsersGroupsSpec {
    let blob = fs::read(&path).unwrap();
    serde_json::from_slice(&blob).unwrap()
}

