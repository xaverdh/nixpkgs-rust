// Nixos specific state files

use std::fs;
use std::str;
use std::path::{Path,PathBuf};
use std::io::Result;

use crate::ids::Id;
use crate::utils;

use std::collections::HashSet;
use std::collections::HashMap;
use serde_json;


pub const NOGROUP_GID : u32 = 65534;
pub const SUBUID_INDEX : u32 = 100000;
pub const UID_COUNT : u32 = 65536;

pub struct NixOSVar {
    nixos : PathBuf
}

impl NixOSVar {

    pub fn new<P : AsRef<Path>>(path : P) -> NixOSVar {
        NixOSVar { nixos: path.as_ref().to_path_buf() }
    }

    // Handling of nixos/declarative-{users,groups}
    // These files remember the declarative users / groups present
    // on the system.

    pub fn write_users(&self, users : &Vec<String>) -> Option<()> {
        write_decl(&self.nixos.join("declarative-users"),users)
    }
    
    pub fn write_groups(&self, groups : &Vec<String>) -> Option<()> {
        write_decl(&self.nixos.join("declarative-groups"),groups)
    }
    
    pub fn read_users(&self) -> Result<HashSet<String>> {
        read_decl(&self.nixos.join("declarative-users"))
    }
    
    pub fn read_groups(&self) -> Result<HashSet<String>> {
        read_decl(&self.nixos.join("declarative-groups"))
    }

    // Handling of (u|g)id map files.
    // These maps keep track of all the user and group ids used in the
    // history of the system (both current as well as deleted ones).
    // The auto subuid map tracks automatically assigned subuids


    pub fn read_uids(&self) -> HashMap<String,Id> {
        read_id_file(&self.nixos.join("uid-map"))
    }
    
    pub fn read_gids(&self) -> HashMap<String,Id> {
        read_id_file(&self.nixos.join("gid-map"))
    }
    
    pub fn read_subuids(&self) -> HashMap<String,Id> {
        read_id_file(&self.nixos.join("auto-subuid-map"))
    }
    
    pub fn write_uids(&self, map : &HashMap<String,Id>){
        write_id_file(&self.nixos.join("uid-map"),map)
    }
    
    pub fn write_gids(&self, map : &HashMap<String,Id>){
        write_id_file(&self.nixos.join("gid-map"),map)
    }
    
    pub fn write_subuids(&self, map : &HashMap<String,Id>){
        write_id_file(&self.nixos.join("auto-subuid-map"),map)
    }
}


// Read user or group id file, and yield a HashMap from names to ids.
// If the file does not exist, yield an empty Map.
fn read_id_file(path : &Path) -> HashMap<String,Id> {
    if path.exists() {
        let blob = fs::read_to_string(path).unwrap();
        serde_json::from_str(&blob).unwrap()
    } else {
        HashMap::new()
    }
}

fn write_id_file(path : &Path, id_map : &HashMap<String,Id>) {
    let data = serde_json::to_string(&id_map).unwrap();
    utils::atomic_write(path, data.as_bytes(), Some(0o644),None,None).unwrap()
}

fn write_decl(file : &Path, data : &Vec<String>) -> Option<()> {
    utils::atomic_write(&file, &data.join(" ").as_bytes(), Some(0o644),None,None)
}

fn read_decl(file : &Path) -> Result<HashSet<String>> {
    if file.exists() {
        let data = fs::read(file)?;
        Ok(parse_decl(&data))
    } else {
        Ok(HashSet::new())
    }
}

fn parse_decl(data : & Vec<u8>) -> HashSet<String> {
    data.split(|c| *c == b' ').map(
        |bs| str::from_utf8(&bs).unwrap().to_string()
        ).collect()
}


