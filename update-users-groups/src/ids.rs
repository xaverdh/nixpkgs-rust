// Handling of (user|group) ids
//
// IdAllocator manages a contiguous range of ids, and allows
// allocating new ids from that range, either by manually
// specifying an id to try, or automatically by trying the
// boundaries of the range (thereby shrinking it).
// It also holds an HashSet of forbidden ids, which can be
// initialized on creating (effectively forbidding those ids)
// and is internally used to track manually allocated ids.
// Finally for convenience, a callback can be specified, which
// checks if an id is invalid for externally known reasons.
use std::ffi::OsString;
use std::collections::HashSet;

pub type Id = u32;

pub struct IdAllocator {
    increment : Id,
    used_ids : HashSet<Id>,
    lower : Id,
    upper : Id,
}

impl IdAllocator {
    pub fn new(
        increment : Id,
        used : HashSet<Id>,
        lower : Id,
        upper : Id,
    ) -> IdAllocator {
        assert!(lower <= upper);
        // Ensure we don't hit limits when allocating.
        assert!(Id::MIN + increment <= lower);
        assert!(upper + increment <= Id::MAX);
        IdAllocator {
            increment: increment,
            used_ids: used,
            lower: lower,
            upper: upper,
        }
    }

    fn is_valid(&self, id : Id) -> bool {
        self.lower <= id && id <= self.upper
            && ! self.used_ids.contains(&id)
    }

    pub fn allocate_up<F : Fn(Id)-> bool>(&mut self, allowed : F)
        -> Option<Id> {
        loop {
            if !self.lower <= self.upper {
                return None
            }
            let id : Id = self.lower;
            unsafe {
                self.lower = self.lower.unchecked_add(self.increment);
            }
            if !self.used_ids.contains(&id) && allowed(id) {
                return Some(id)
            }
        }
    }

    pub fn allocate_down<F : Fn(Id) -> bool>(&mut self, allowed : F)
        -> Option<Id> {
        loop {
            if !self.lower <= self.upper {
                return None
            }
            let id : Id = self.upper;
            unsafe {
                self.upper = self.upper.unchecked_sub(self.increment);
            }
            if !self.used_ids.contains(&id) && allowed(id) {
                return Some(id)
            }
        }
    }

    pub fn try_allocate(&mut self, id : Id) -> Option<Id> {
        if self.is_valid(id) {
            self.used_ids.insert(id);
            Some(id)
        } else {
            None
        }
    }
}

pub fn parse_id(s : &str) -> Option<Id>{
    u32::from_str_radix(s, 10).ok()
}

